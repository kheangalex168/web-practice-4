import { Card, Button } from "react-bootstrap";
import { Component } from "react";

class MyCard extends Component {
  constructor() {
    super();
    this.state = [
      {
        id: 1,
        title: "cat",
        img: "https://i.ytimg.com/vi/1Ne1hqOXKKI/maxresdefault.jpg",
      },
      {
        id: 2,
        title: "dog",
        img: "https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-1100x628.jpg",
      },
      {
        id: 3,
        title: "cow",
        img: "https://cdn.britannica.com/55/174255-050-526314B6/brown-Guernsey-cow.jpg",
      },
    ];
  }

  handleClick = (i) => {
    document.getElementById(i).style.display = "none";
  };

  render() {
    let MyCard = this.state.map((item, i) => {
      return (
        <Card
          style={{
            width: "18rem",
          }}
          id={i}
        >
          <Card.Img variant="top" src={item.img} />
          <Card.Body>
            <Card.Title>{item.title}</Card.Title>
            <Button variant="danger" onClick={() => this.handleClick(i)}>
              delete
            </Button>
          </Card.Body>
        </Card>
      );
    });

    return <div>{MyCard}</div>;
  }
}

export default MyCard;
