import '../App.css';
import React from 'react';
import MyCard from './MyCard';
import MyNavbar from './MyNavbar';
import 'bootstrap/dist/css/bootstrap.min.css';


function MyApp() {
  return (
    <div className="MyApp">
      <MyNavbar />
      <MyCard />
    </div>
  );
}

export default MyApp;
